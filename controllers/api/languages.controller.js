var config = require('config.json');
var express = require('express');
var router = express.Router();
var languageService = require('services/language.service');

// routes
router.put('/create', createLanguage);
router.get('/all', getAllLanguages);
router.get('/getById', getLanguage);
router.put('/:_id', updateLanguage);
router.delete('/:_id', deleteLanguage);

module.exports = router;



function createLanguage(req, res) {


    languageService.create(req.body)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getLanguage(req, res) {
    languageService.getById(req.language.sub)
        .then(function (language) {
            if (language) {
                res.send(language);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getAllLanguages(req, res) {
    languageService.getAll()
        .then(function (languages) {
            if (languages) {
                res.send(languages);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function updateLanguage(req, res) {
    var languageId = req.language.sub;

    languageService.update(languageId, req.body)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function deleteLanguage(req, res) {
    var languageId = req.language.sub;


    languageService.delete(languageId)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

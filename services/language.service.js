var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('languages');

var service = {};

service.getById = getById;
service.getAll = getAll;
service.create = create;
service.update = update;
service.delete = _delete;

module.exports = service;

function getById(_id) {
    var deferred = Q.defer();

    db.languages.findById(_id, function (err, language) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (language) {
            // return language
            deferred.resolve(language);
        } else {
            // language not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getAll() {
    var deferred = Q.defer();

    db.languages.find().toArray(function (err, languages) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (languages) {
            // return languages
            deferred.resolve(languages);
        } else {
            // languages not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}



function create(languageParam) {
    var deferred = Q.defer();

    // validation
    db.languages.findOne(
        { name: languageParam.name },
        function (err, language) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (language) {
                // language already exists
                deferred.reject('Language "' + languageParam.name + '" already exists');
            } else {
                createLanguage();
            }
        });

    function createLanguage() {
        // set language object to languageParam without the cleartext password
        var language = languageParam;


        db.languages.insert(
            language,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function update(_id, languageParam) {
    var deferred = Q.defer();

    // validation
    db.languages.findById(_id, function (err, language) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (language.name !== languageParam.name) {
            // language name has changed so check if the new name is already taken
            db.languages.findOne(
                { name: languageParam.name },
                function (err, language) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (language) {
                        // language already exists
                        deferred.reject('Language "' + languageParam.name + '" already exists')
                    } else {
                        updateLanguage();
                    }
                });
        } else {
            updateLanguage();
        }
    });

    function updateLanguage() {
        // fields to update
        var set = {
            name: languageParam.name
        };



        db.languages.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    db.languages.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}

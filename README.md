# TandemRadar

Small MEAN stack project for Informations Systems Development at FER, University 
of Zagreb. 

The basic idea is for people to find a tandem partner for learning languages. 
Users sign up on the TandemRadar website and if they find someone that fits, 
they pay a small fee to get the other person's contact info. Additionally, 
there will be ads for language courses of language institutes and language study 
travel.

The project has a registration and login function. The user can see a set of 
other users that are available for a tandem pairing. The catalogue of other 
users can be filtered and users can search. For the catalogue, there is a 
master-detail view e.g. the list only shows the most important info, but users
can click to see the full profile of another TandemRadar user. Every user can 
edit the user's own profile (CRUD). 
The functionality of requesting tandem pairings is not implemented yet. 
The web app is responsive using the Bootstrap grid. 
 
## Prerequisites

To run this project locally, you need npm, mongodb and Node.js installed. 



## How to start the application
Start node.js:

node server.js


Start mongodb: sudo service mongod start (sudo service mongod stop)

when setting up the project for the first time, run in the mongo shell:

db.createCollection("languages", { capped : true, size : 5242880, max : 5000 } )

db.languages.insert( { name: "English" } )

db.languages.insert( { name: "Spanish" } )

db.languages.insert( { name: "German" } )

db.languages.insert( { name: "Mandarin" } )

db.languages.insert( { name: "Croatian" } )

db.languages.insert( { name: "Russian" } )

db.languages.insert( { name: "Turkish" } )

db.languages.insert( { name: "Italian" } )

db.languages.insert( { name: "French" } )


open in browser at: http://localhost:3000/

### Useful mongodb commands

Start shell: mongo --host 127.0.0.1:27017

show dbs

use your_database_name

show collections

db.collection_name.find()
or
db.collection_name.find().pretty()

db.collection_name.drop()

## Directory structure
tree generated with: https://github.com/michalbe/md-file-tree


- __tandemradar__
  - [component diagram.pdf](tandemradar/component diagram.pdf)  --- component diagram of app
  - [README.md](tandemradar/README.md)  --- this file
  - __app__
    - [index.html](tandemradar/app/index.html)  --- view of landing page of the app
    - [app.js](tandemradar/app/app.js)  --- the app
    - __app-services__
      - [flash.service.js](tandemradar/app/app-services/flash.service.js)  --- service for error and success messages
      - [language.service.js](tandemradar/app/app-services/language.service.js)  --- service for languages
      - [user.service.js](tandemradar/app/app-services/user.service.js)  --- service for users
    - __catalogue__
      - [index.controller.js](tandemradar/app/catalogue/index.controller.js)  --- controller for tandem pairings catalogue
      - [index.html](tandemradar/app/catalogue/index.html)  --- view for catalogue
    - __css__
      - [custom.css](tandemradar/app/css/custom.css)  --- custom CSS
      - [templatemo-style.css](tandemradar/app/css/templatemo-style.css)  --- CSS from template
    - __detail__
      - [index.controller.js](tandemradar/app/detail/index.controller.js)  --- controller for detail page for showing details about another user profile
      - [index.html](tandemradar/app/detail/index.html)  --- view for detail page
    - __img__
      - [linda.jpg](tandemradar/app/img/linda.jpg)  --- placeholder photo
    - __pairings__
      - [index.html](tandemradar/app/pairings/index.html)  --- view for showing existing tandem pairings
      - [index.controller.js](tandemradar/app/pairings/index.controller.js)  --- controler for showing existing tandem pairings
    - __profile__
      - [index.controller.js](tandemradar/app/profile/index.controller.js)  --- controller for user profile (CRUD)
      - [index.html](tandemradar/app/profile/index.html)  --- view for user profile
  - __controllers__
    - __api__
      - [languages.controller.js](tandemradar/controllers/api/languages.controller.js)  --- API controller for languages (handles requests)
      - [users.controller.js](tandemradar/controllers/api/users.controller.js)  --- API controller for users (handles requests)
    - [app.controller.js](tandemradar/controllers/app.controller.js)  --- general app controller
    - [login.controller.js](tandemradar/controllers/login.controller.js)  --- login controller (sends requests)
    - [register.controller.js](tandemradar/controllers/register.controller.js)  --- register controller (sends requests)
  - [config.json](tandemradar/config.json)
  - [node_modules](tandemradar/node_modules)
  - [package.json](tandemradar/package.json)
  - [server.js](tandemradar/server.js)
  - __services__
    - [language.service.js](tandemradar/services/language.service.js)  --- language service (using mongodb)
    - [user.service.js](tandemradar/services/user.service.js)  --- user service (using mongodb)
  - __views__
    - [login.ejs](tandemradar/views/login.ejs)  --- view for login
    - [register.ejs](tandemradar/views/register.ejs)  --- view for registration
    - __partials__
      - [footer.ejs](tandemradar/views/partials/footer.ejs)  --- footer
      - [header.ejs](tandemradar/views/partials/header.ejs)  --- header



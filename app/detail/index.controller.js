﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('Detail.IndexController', Controller);

    function Controller(UserService, $stateParams) {
        var vm = this;

        vm.user = null;
        vm.viewed_user = null;


        initController();



        function initController() {
            // get current user
            UserService.GetCurrent().then(function (user) {
                vm.user = user;
            });

            UserService.GetById($stateParams._id).then(function (viewed_user) {
              vm.viewed_user = viewed_user;

            });



        }
    }

})();

﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('Catalogue.IndexController', Controller);

    function Controller(UserService, LanguageService) {
        var vm = this;

        vm.user = null;
        vm.users = null;
        vm.languages = null;
        vm.selectedLanguage = null;

        vm.filterLanguages = function(user) {
                if(vm.selectedLanguage == null){
                  return true;
                }
                return (user.spokenLanguages.toLowerCase().includes(vm.selectedLanguage.name.toLowerCase())
                  || user.interestingLanguages.toLowerCase().includes(vm.selectedLanguage.name.toLowerCase()));
            };


        initController();



        function initController() {
            // get current user
            UserService.GetCurrent().then(function (user) {
                vm.user = user;
                // get all users
                UserService.GetAll().then(function (users) {
                    vm.users = users;

                    for (var i = users.length - 1; i >= 0; --i) {
                        if (vm.users[i]._id == vm.user._id) {
                            vm.users.splice(i,1);
                        }
                    }
                });

            });

            LanguageService.GetAll().then(function (languages) {
                vm.languages = languages;
            });


        }
    }

})();

﻿(function () {
    'use strict';

    angular
        .module('app', ['ui.router'])
        .config(config)
        .run(run);

    function config($stateProvider, $urlRouterProvider) {
        // default route
        $urlRouterProvider.otherwise("/");

        $stateProvider
            .state('catalogue', {
                url: '/',
                templateUrl: 'catalogue/index.html',
                controller: 'Catalogue.IndexController',
                controllerAs: 'vm',
                data: { activeTab: 'catalogue' }
            })
            .state('pairings', {
                url: '/pairings',
                templateUrl: 'pairings/index.html',
                controller: 'Pairings.IndexController',
                controllerAs: 'vm',
                data: { activeTab: 'pairings' }
            })
            .state('profile', {
                url: '/profile',
                templateUrl: 'profile/index.html',
                controller: 'Profile.IndexController',
                controllerAs: 'vm',
                data: { activeTab: 'profile' }
            })
            .state('detail', {
              url: '^/:_id',
              templateUrl: 'detail/index.html',
              controller: 'Detail.IndexController',
              controllerAs: 'vm',
              data: { activeTab: 'catalogue' },
              params : {
                obj : null
              },
            });

    }

    function run($http, $rootScope, $window) {
        // add JWT token as default auth header
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + $window.jwtToken;

        // update active tab on state change
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            $rootScope.activeTab = toState.data.activeTab;
        });
    }

    // manually bootstrap angular after the JWT token is retrieved from the server
    $(function () {
        // get JWT token from server
        $.get('/app/token', function (token) {
            window.jwtToken = token;

            angular.bootstrap(document, ['app']);
        });
    });
})();

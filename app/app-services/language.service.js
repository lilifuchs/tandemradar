﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('LanguageService', Service);

    function Service($http, $q) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;

        return service;



        function GetAll() {
            return $http.get('/api/languages/all').then(handleSuccess, handleError);
        }

        function GetById(_id) {
            return $http.get('/api/languages/' + _id).then(handleSuccess, handleError);
        }

        function Create(langaue) {
            return $http.post('/api/languages', language).then(handleSuccess, handleError);
        }

        function Update(language) {
            return $http.put('/api/languages/' + language._id, language).then(handleSuccess, handleError);
        }

        function Delete(_id) {
            return $http.delete('/api/languages/' + _id).then(handleSuccess, handleError);
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(res) {
            return $q.reject(res.data);
        }
    }

})();
